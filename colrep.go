package main

import (
	"fmt"
	ex "github.com/xuri/excelize"
	"log"
	"regexp"
	"strconv"
)

func main() {

	col := "g"
	// regex := `^cat`
	regex := `/dev/vda9              \d{2}M`
	to := "/dev/vda9              100M"
	// to := `hoge`
	index := 1 // sheet index
	bookName := "commandList.xlsx"

	re := regexp.MustCompile(regex)

	book, errO := ex.OpenFile(bookName)
	if errO != nil {
		log.Fatalln(errO)
	}

	sheet := book.GetSheetName(index)

	for i, v := range book.GetRows(sheet) {
		if re.MatchString(v[ex.TitleToNumber(col)]) {
			fmt.Println("matched")
			result := re.ReplaceAllString(v[ex.TitleToNumber(col)], to)
			book.SetCellValue(sheet, col+strconv.Itoa(i+1), result)
		}
	}

	errS := book.SaveAs("done.xlsx")
	if errS != nil {
		log.Fatalln(errS)
	}
}
