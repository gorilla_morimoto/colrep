package main

import (
	"flag"
	"strconv"
	"regexp"
	"log"
	ex "github.com/xuri/excelize"
)

func main() {

	var (
		col string
		regex string
		to string
	)

	flag.StringVar(&col, "c", "", "target column")
	flag.StringVar(&regex, "r", "", "regex")
	flag.StringVar(&to, "t", "", "replace to")
	
	flag.Parse()

	if col == "" {
		log.Fatalln("error: -c is necessary")
	} else if regex == "" {
		log.Fatalln("error: -r is necessary")
	} else if to == "" {
		log.Fatalln("error: -t is necessary")
	}

	re, errR := regexp.Compile(regex)
	if errR != nil {
		log.Fatalln("error: invalid regex")
	}

	book, errO := ex.OpenFile("commandList.xlsx")
	if errO != nil {
		log.Fatalln(errO)
	}

	sheet := book.GetSheetName(1)
	

	for i, v := range book.GetRows(sheet) {
		if re.MatchString(v[ex.TitleToNumber(col)]) {
			result := re.ReplaceAllString(v[ex.TitleToNumber(col)], to)
			book.SetCellValue(sheet, col + strconv.Itoa(i+1), result)
		}
	}

	errS := book.SaveAs("done.xlsx")
	if errS != nil {
		log.Fatalln(errS)
	}
}